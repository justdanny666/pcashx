# General - Configuration
spring:
  main:
    banner-mode: off

  cloud:
    loadbalancer:
      retry:
        retry-on-all-operations: true
    cloud:
      discovery:
        enabled: true
      gateway:
        discovery:
          locator:
            enabled: true
            lower-case-service-id:
              enabled: true

  # Boot Admin - Configuration
  boot:
    admin:
      client:
        username: admin
        password: admin
        url: http://service-registry.pcashx.svc:8761
        instance:
          url: http://service-registry.pcashx.svc:8761
          prefer-ip: true
          metadata:
            user:
              name: ${spring.security.user.name}
              password: ${spring.security.user.password}
      url: http://service-registry.pcashx.svc:8761
      username: admin
      password: admin
  rabbitmq:
    host: 192.168.150.155
    port: 5672
    username: default_user_FaOSbEbAhQ24dbmqk5H
    password: V8uCn4wW7ovI5_7lblFFAxYOqifqwkvr

  # Datasource - Configuration
  datasource:
    url: jdbc:postgresql://192.168.150.156:5432/pcashx?currentSchema=agcm
    username: postgres
    password: bDnv91NGfrOOPd1l

    hikari:
      minimum-idle: 5
      maximum-pool-size: 25
      validation-timeout: 300000
  jpa:
    hibernate:
      ddl-auto: none
    # Tune startup
    database-platform: org.hibernate.dialect.PostgreSQLDialect
    properties:
      hibernate:
        temp:
          use_jdbc_metadata_defaults: false

  # Liquibase - Configuration
  liquibase:
    change-log: classpath:db/changelog/db.changelog-master.xml
    url: jdbc:h2:tcp://localhost/~/agcm
    user: sa
    password: sa
    drop-first: true

# JWT - Configuration
pcash:
  server-mode: production
  jwt:
    expiration: 30
    issuer: pcash
    signSecret: qwertyuiopasdfghjklzxcvbnmqwerty
    storage: database
  web:
    public-paths:
      - /actuator/**
      - /favicon.ico

# Eureka Client - Configuration
eureka:
  instance:
    prefer-ip-address: true
    appname: ${spring.application.name}
    hostname: service-registry.pcashx.svc
    health-check-url-path: /actuator/health
    lease-renewal-interval-in-seconds: 10
  client:
    enabled: true
    healthcheck:
      enabled: true
    register-with-eureka: true
    fetch-registry: true
    registry-fetch-interval-seconds: 5
    service-url:
      defaultZone: http://service-registry.pcashx.svc:8761/eureka/

# Actuator - Configuration
management:
  endpoints:
    web:
      exposure:
        include: "*"
  endpoint:
    health:
      show-details: always

# Logging - Configuration
logging:
  level:
    ROOT: info
    liquibase: error
  file:
    name: ../logs/${spring.application.name}/${spring.application.name}.log
    # name: /nfs/data/logs/${spring.application.name}/${spring.application.name}.log #open if using NFS
  pattern:
    file: "%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(%5p) %clr(${PID}){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n%wEx"

# Feign Configuration
feign:
  autoconfiguration:
    jackson:
      enabled: true
